# Intro ML Course - Computer Assignments

This is the repository for my solutions to Introduction to Machine Learning course coumputer assignments at Sharif University of Technology. This course was taught by Prof. Jamal Golestani at the Department of Electrical Engineering. This course was focused on Learning Theory.

For all of the assignments, there exists a PDF report file in Persian.

<br><br><b>Computer Assignment #1:</b><br>
Implementation of Linear Regression Algorithms.

<br><br><b>Computer Assignment #3:</b><br>
Implementation of Linear Classification Algorithms.

<br><br><b>Computer Assignment #6:</b><br> Using Multi-Layer Perceptron. The effect of the number of hidden layers, number of neurons and the optimization algorithm was investigated.

<br><br><b>Computer Assignment #8:</b><br> Implementation of K-Means. We also used serveral classification method to classify the Fashion-MNIST dataset and compared the results.
